# **_Speech-to-Text_**

_This project was made by **Ashutosh Mukerji**, **Dmitrii Viatkin** and **Jakub Romanowski** as a project using Agile methodology for the course Software Studio II under the supervision of **Igor Wojnicki**._

**Project Goals:**
==========================================================================

Design and implement software that provides a text transcription from an audio stream of file. Possible application is to provide transcripts from interviews, interrogations, psychotherapy sessions etc. Assuming that:

- audio stream has to be securely stored,
- the transcript has to be a timestamped text in relation to the original audio,
- the transcript has to be securely stored,
- the software has to work offline,
- the audio or text cannot be transmitted.

Using already available, ready to use software libraries.

**See also**:
- [_Requirements_](https://gitlab.com/mukeashu/speech-to-text/-/blob/main/Requirements.md)
- [_Installation and Configuration_](https://gitlab.com/mukeashu/speech-to-text/-/blob/main/Install&Config.md)
- [_User Manual_](https://gitlab.com/mukeashu/speech-to-text/-/blob/main/User%20Manual.md)
- [_Team member Responsibilities_](https://gitlab.com/mukeashu/speech-to-text/-/blob/main/Responsibilities.md)




>     Speech-to-Text 
>     Copyright (C) 2022  Ashutosh Mukerji, Dmitrii Viatkin, Jakub Romanowski 
> 
>     This program is free software: you can redistribute it and/or modify
>     it under the terms of the GNU General Public License as published by
>     the Free Software Foundation, either version 3 of the License, or
>     (at your option) any later version.
> 
>     This program is distributed in the hope that it will be useful,
>     but WITHOUT ANY WARRANTY; without even the implied warranty of
>     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
>     GNU General Public License for more details.
> 
>     You should have received a copy of the GNU General Public License
>     along with this program.  If not, see <https://www.gnu.org/licenses/>.
> 
>     Contact Info: mukerji@student.agh.edu.pl, jromanow@student.agh.edu.pl, viatkin@student.agh.edu.pl
[LICENSE](https://gitlab.com/mukeashu/speech-to-text/-/blob/main/LICENSE)
