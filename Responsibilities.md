_**Team Member Responsibilities:**_
==========================================================================

- Speech_recodnition installation: Dmitrii Viatkin
- Transciption functionalities: Jakub Romanowski
- Code skeleton design: Jakub Romanowski
- Interactive Menu: Ashutosh Mukerji
- Command line menu: Jakub Romanowski
- Database functionalities: Jakub Romanowski
- Timestamps: Dmitrii Viatkin 
- Code functionality: Ashutosh Mukerji, Jakub Romanowski
- Basic software operability: Ashutosh Mukerji, Dmitrii Viatkin, Jakub Romanowski
- Basic Research: Ashutosh Mukerji, Dmitrii Viatkin, Jakub Romanowski
- Database Encryption Research: Ashutosh Mukerji, Jakub Romanowski
- Database Indexing: Jakub Romanowski
- Efficient search and highlights: Jakub Romanowski
- Testing: Ashutosh Mukerji, Dmitrii Viatkin, Jakub Romanowski
- Soundfile conversion: Dmitrii Viatkin 
- Audio samples collection: Dmitrii Viatkin
- Documentation: Ashutosh Mukerji, Jakub Romanowski
- Troubleshooting: Jakub Romanowski, Ashutosh Mukerji
- Licensing: Ashutosh Mukerji

