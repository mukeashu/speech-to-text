_**User Manual:**_
==========================================================================

The following are the steps to successfully run the software program after following all the initial instructions from [here](https://gitlab.com/mukeashu/speech-to-text/-/blob/main/Install&Config.md) :


The program uses file paths as primary keys to ensure no duplicates in the database. Attempting to save the same file to the database more than once will result in errors.

When using the display functionality the file path has to be matched exactly.


**Command line interface:**
==========================================================================

Run the following code on your command-line terminal: `python main.py [OPTION] [TARGET]`

[OPTION] includes:

-t, transcribe  
>in this case the [TARGET] has to be the relative path to the file that needs to be transcribed. The program will display the transciption and prompt the user to decide whether the transcription should be saved in the databse. User has to select either [y/n]. Since files utilize

-s, search
>in this case [TARGET] is the phrase to be searched in the transcriptions in the database. If it contains spaces it needs to be written within parenthesis. The program will display a list of files containg the searched phrase along with a fragment of the transcription where it is located.

-h, help
>this flag does not require an additional argument. It displays helpful information about how to run the script.

-d, display
>in this case [TARGET] is the relative path to the file provided when transcribing it. the program will fetch the transcription of the file from the database if it exists. 


**Interactive mode:**
==========================================================================

1. Run the program by either pressing the 'Play' button on your IDE or, run the following code on your command-line terminal:
```
python main.py
python database.py
```

2. Choose the first option '**_Transcribe_**' from the menu shown and enter the path to the .wav audio file( in our test case _audio_files/test.wav_).

**WARNING:** The supported formats for this program are *ADPCM* and *PCM* .wav files. The program may work with other formats of .wav files, but is has not been tested yet.

3. Save the text file obtained by pressing '**_Y/y_**'. Pressing '**_N/n_**' will not store the transcription in the database.
4. To search for a phrase from the transcription obtained, chose the second option '**_Search_**'.
5. Enter the phrase wanted to search for.
6. The result will be displayed on the screen.
7. To display the transcript choose the third option '**_Display Transcript_**' and enter the path to the file.
8. Choose the fourth option '**_Quit_**' to exit the program.
