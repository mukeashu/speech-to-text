import contextlib
import wave
import database
import sys
import speech_recognition as sr
import soundfile


def print_menu():
    print("[1] Transcribe.")
    print("[2] Search.")
    print("[3] Display Transcript.")
    print("[4] Quit.")


def display_transcript(filename):
    print(filename)
    print(database.display_file(filename))


def menu():
    print_menu()
    user_input = 0

    while user_input != 4:

        user_input = int(input("Choose desired action from list: "))

        if user_input == 1:
            file = input("Please provide relative path to the file:")
            transcribe(file)

        elif user_input == 2:
            phrase = input("Please provide a phrase to search:")
            search(phrase)

        elif user_input == 3:
            file = input("Please provide relative path to the recording you want to find:")
            display_transcript(file)

        elif user_input == 4:
            print("Exiting...")

        else:
            print("Invalid Selection. Try again!")


def parse_input():
    commands = ["transcribe", "-t", "search", "-s", "help", "-h", "display", "-d"]
    target = "audio_files/test.wav"
    flag = sys.argv[1]
    if len(sys.argv) > 2:
        flag = sys.argv[1]
        target = sys.argv[2]
    if flag not in commands:
        print("Input incorrect, cannot be parsed")
    if flag in commands[:2]:
        transcribe(target)
    if flag in commands[2:4]:
        search(target)
    if flag in commands[4:6]:
        print("Run the script in command line with a function flag that includes: ")
        print(commands)
        print("The transcribe, display and search flag also require a second argument")
        print("Alternatively execute without additional arguments to use interactive mode")
        print("See User Manual.md for more information.")
    if flag in commands[6:]:
        display_transcript(target)
    if flag not in commands:
        print("Incorrect flag. Please use interactive mode")
        menu()


def transcribe(filename):
    new_filename = converter(filename)
    raw_text = parse_audio(new_filename)
    print(raw_text)
    print("Added timestamps as follows: ")
    timestamped = add_timestamps(new_filename, raw_text)
    print(timestamped)
    choice = input("Do you want to save to database? [y/n]")
    if choice in ['y', "Y"]:
        database.save_to_database(filename, timestamped)
    else:
        print("Transcription not saved")


def search(phrase):
    records = database.search_database(phrase)
    if records is None:
        print("Phrase not found")
        return
    for record in records:
        print("In file: " + record[0] + " found phrase: ")
        print(record[2])


def converter(filename):
    data, samplerate = soundfile.read(filename)
    new_filename_split = filename.split('/')
    new_filename_split[1] = "converted_" + new_filename_split[1]
    new_filename_split = '/'.join(new_filename_split)
    soundfile.write(new_filename_split, data, samplerate, subtype='PCM_16')
    return new_filename_split


def parse_audio(filename):
    recognizer = sr.Recognizer()
    test = sr.AudioFile(filename)
    with test as source:
        audio = recognizer.record(source)
    raw_text = recognizer.recognize_sphinx(audio)
    return raw_text


def add_timestamps(filename, raw_text):
    # According to https://stackoverflow.com/questions/7833807/get-wav-file-length-or-duration, the easiest way to
    # get the file duration is to split
    with contextlib.closing(wave.open(filename, 'r')) as f:
        frames = f.getnframes()
        rate = f.getframerate()
        file_duration = frames / float(rate)
        words = raw_text.split(' ')  # splits the string into parts divided by space
        num_of_words = len(words)
        if file_duration < 30.0:
            half_timing = int(file_duration / 2)
            mid_pos = num_of_words // 2
            time_str = "(#" + str(half_timing) + "sec)"
            raw_text_2 = words[:mid_pos] + [time_str] + words[mid_pos:]
            raw_text_2 = ' '.join(raw_text_2)
        else:
            steps = int(file_duration / 15)
            start_position = num_of_words // steps
            position = 0
            timing = 0
            raw_text_2 = words
            for i in range(steps):
                position = position + start_position
                timing = timing + 15
                time_str = "(#" + str(timing) + "sec)"
                raw_text_2 = raw_text_2[:position] + [time_str] + raw_text_2[position:]
            raw_text_2 = ' '.join(raw_text_2)
    return raw_text_2


def main():
    database.main()
    if len(sys.argv) == 1:
        menu()
    else:
        parse_input()


if __name__ == "__main__":
    main()
