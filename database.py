import psycopg2


def connect():
    try:
        conn = psycopg2.connect(
            host="localhost",
            database="transcriptions",
            user="program",
            password="jp2s0F4",
            port="5432"
        )
        return conn
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
        return None


def configure(conn):
    cur = conn.cursor()
    cur.execute('CREATE TABLE IF NOT EXISTS transcripts('
                'filename TEXT NOT NULL, '
                'transcript TEXT NOT NULL, '
                'tsv TSVECTOR NOT NULL, '
                'PRIMARY KEY (filename));')
    cur.execute('CREATE INDEX IF NOT EXISTS tsv_idx ON transcripts USING gin (tsv);')
    conn.commit()
    cur.close()
    conn.close()



def search_database(phrase):
    # alternative query SELECT to_tsvector('The quick brown fox jumped over the lazy dog.');
    # produces a lexeme with indexes of meaningful words in the phrase
    sql = "SELECT filename, transcript, ts_headline('english', transcript, plainto_tsquery('{0}')) FROM transcripts WHERE tsv @@ to_tsquery( 'english', '{0}');"
    words = phrase.split()
    query = " & ".join(words)
    conn = connect()
    try:
        cur = conn.cursor()
        cur.execute(sql.format(query))
        results = []
        while True:
            rows = cur.fetchall()
            if not rows:
                break
            for row in rows:
                results.append(row)
        cur.close()
        conn.close()
        return results
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
        return None


def display_file(file):
    sql = "SELECT transcript FROM transcripts WHERE filename ='{0}';"
    conn = connect()
    try:
        cur = conn.cursor()
        cur.execute(sql.format(file))
        result = cur.fetchone()
        cur.close()
        conn.close()
        return result
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
        return None


def save_to_database(filename, trans):
    sql = "INSERT INTO transcripts (filename, transcript, tsv) VALUES (%s, %s, to_tsvector('english', %s));"
    conn = connect()
    try:
        cur = conn.cursor()
        cur.execute(sql, (filename, trans, trans))
        conn.commit()
        cur.close()
        conn.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)


def main():
    conn = connect()
    configure(conn)
    conn.close()


if __name__ == "__main__":
    main()
