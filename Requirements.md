**Requirements:**
==========================================================================

1. Audio stream should be stored securely
2. Transcripts should be stored securely
3. The transcript should be time stamped according to the audio file (regular)
4. The software has to work offline
5. No transmission of files to third parties should happen
6. Transcripts should be indexed for efficient searching
7. The software will solely transcribe English audio recordings

**Fulfilment of Requirements:**
==========================================================================

_The status of the requirements are described in detail below:_

- [ ] Audio stream and Transcripts should be stored securely : ( _Partial_ ) Unfinished due to time constraints 
- [x] The transcript should be time stamped according to the audio file (regular) 
- [x] The software has to work offline
- [x] No transmission of files to third parties should happen
- [x] Transcripts should be indexed for efficient searching 
- [x] The software will solely transcribe English audio recordings




Our team has decided to utilize Python for this project, a language we had no prior experience with. We've made that choice based off of the vast amount of suitable libraries available in Python as well as due to it being a language used in many of our other classes this semester. 

In the end, most of the libraries turned out to be unusable due to our requirement of not sending any confidential information over the network. 

Innitially the team had serious problems with installing necessary libraries which slowed the progress but were resolved in reasonable time.

A somewhat underestimated hindrance was the difference in design paradigms and structure between Python and all of our previously known, lower level languages. As a result a large amount of time was spent researching and learning basic use of the Python language which certainly contributed to long period of no substantial, measurable progress on the project. An additional issue was the lack of strict structure of weekly meetings, primarily caused by short sighted focus on more immediately pressing academic matters of the team members. 

Regardless we have managed to complete a working prototype of the assigned program which though lacking important security features shows promise in terms of demonstrating the basic functionalities from the requirement.

With a little more time for developement, and perhaps a more precise transcription library) we believe the project could be utilized commercially.
